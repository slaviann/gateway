import unittest
import sys
import unittest
import logging
import datetime
import hashlib

logging.basicConfig(level=logging.INFO)
logging.getLogger('suds.client').setLevel(logging.INFO)
logging.getLogger('suds.transport').setLevel(logging.INFO)

from sqlalchemy.engine import Engine
from zeep import Client

from gateway.test.base_test import BaseTest
from sqlalchemy import create_engine, Table, MetaData, select


class TestStringMethods(BaseTest):

    @property
    def generateAuthHash(self):
        utc = datetime.datetime.utcnow()
        s = "Gp11V7@dpv:%s%s%s:%s" % (utc.year, utc.month, utc.day, utc.hour)
        h = hashlib.sha256()
        h.update(s.encode('utf-8'))
        return h.hexdigest()

    def test_connect(self):
        client = Client('https://wallet.advcash.com/wsm/merchantWebService?wsdl')
        result = client.service.getBalances(
            {
                'apiName': 'client-api',
                'authenticationToken': self.generateAuthHash,
                'systemAccountName': 'slaviann@gmail.com'
            }
        )
        print(result)


if __name__ == '__main__':
    unittest.main()
