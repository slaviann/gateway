import logging
import sys
import unittest


class BaseTest(unittest.TestCase):

    def __init__(self, *args, **kwargs):
        logger = logging.getLogger()
        logger.level = logging.DEBUG
        logger.addHandler(logging.StreamHandler(sys.stdout))
        super(BaseTest, self).__init__(*args, **kwargs)
