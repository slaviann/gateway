import unittest
import sys
import unittest
import logging

from sqlalchemy.engine import Engine

from gateway.test.base_test import BaseTest
from sqlalchemy import create_engine, Table, MetaData, select


class TestStringMethods(BaseTest):

    def __init__(self, *args, **kwargs):
        logger = logging.getLogger()
        logger.level = logging.DEBUG
        logger.addHandler(logging.StreamHandler(sys.stdout))
        self.engine = create_engine('postgresql://gateway:gateway@localhost:5432/gateway')
        self.conn: Engine = self.engine.connect()
        self.wallet_table: Table = Table('wallet', MetaData(), autoload=True, autoload_with=self.engine, schema="gtw")
        super(TestStringMethods, self).__init__(*args, **kwargs)

    def setUp(self):
        print("hello")

    def test_select_with_reflection(self):
        [print(c.name) for c in self.wallet_table.columns]
        s = select([self.wallet_table])
        [print(r) for r in self.conn.execute(s)]


if __name__ == '__main__':
    unittest.main()
