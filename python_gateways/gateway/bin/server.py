from flask import Flask
from flask_httpauth import HTTPBasicAuth
from werkzeug.security import generate_password_hash, check_password_hash
from flask_cors import CORS, cross_origin

app = Flask(__name__)
CORS(app,  resources={r"/*": {"origins": "*"}})
auth = HTTPBasicAuth()


users = {
    "slaviann": generate_password_hash("taracan100"),
    "susan": generate_password_hash("bye")
}


@auth.verify_password
def verify_password(username, password):
    if username in users and check_password_hash(users.get(username), password):
        return username


@app.route('/')
@cross_origin()
def hello_world():
    return 'Hello World!'


@app.route('/api/hello')
@cross_origin()
@auth.login_required
def index():
    return "Hello, {}!".format(auth.current_user())


if __name__ == '__main__':
    app.run()
