import setuptools

# import os
# os.system("export PYWAY_DBMS=postgres")


setuptools.setup(
    name="paxful-gateways", # Replace with your own username
    version="0.0.1",
    author="paxful team",
    author_email="author@example.com",
    description="A small example package",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    install_requires=[
        'Flask',
        'sqlacodegen',
        'psycopg2',
        'flask_httpauth',
        'flask-cors',
        'zeep',
        'aiohttp',
        'attrs'
    ],
    python_requires='>=3.9',
    scripts=['gateway/bin/server.py']
)
