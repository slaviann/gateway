create schema if not exists gtw;

create table gtw.wallet (
    id bigserial not null PRIMARY KEY,
    balance numeric not null default 0
);

create table gtw.currency (
    id serial not null PRIMARY KEY,
    key varchar(256) not null
);

create table gtw.external_gateway (
    id serial not null PRIMARY KEY,
    name varchar(256) not null,
    enabled boolean not null default true
);

insert into external_gateway(name) values ('flutterwave');


create type gtw.io_states as enum ('new', 'failed', 'proccessed');


-- input interface for service
-- data from input webhooks
create table gtw.input (
    id bigserial not null PRIMARY KEY,
    gateway_id int not null references gtw.external_gateway(id),
    for_user_email varchar(2048) not null,
    fiat_amount numeric not null,
    fiat_currency varchar(1024),
    state gtw.io_states not null default 'new',
    processed timestamp,
    processed_rate numeric
);

create table gtw.output (
    id bigserial not null PRIMARY KEY,
    gateway_id int not null references gtw.external_gateway(id),
    to_external_account jsonb not null,
    output_rate numeric not null,
    fiat_amount numeric not null,
    fiat_currency varchar(1024),
    status gtw.io_states not null,
    enabled boolean default true
);

create table gtw.input_state_changes (
    id bigserial not null PRIMARY KEY,
    "date" timestamp not null default now(),
    input_id bigint not null references gtw.input(id),
    from_state gtw.io_states not null,
    to_state gtw.io_states not null
);

create table gtw.output_state_changes (
    id bigserial not null PRIMARY KEY,
    "date" timestamp not null default now(),
    input_id bigint not null references gtw.output(id),
    from_state gtw.io_states not null,
    to_state gtw.io_states not null
);