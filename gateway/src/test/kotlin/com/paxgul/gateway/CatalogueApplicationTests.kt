package com.paxgul.gateway

import com.google.gson.JsonParser
import com.paxgul.gateway.bl.core.FlutterWavePayoutStrategy
import com.paxgul.gateway.bl.core.PayToDto
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest


@SpringBootTest
class CatalogueApplicationTests {

    @Autowired
    private lateinit var flutterWavePayoutStrategy: FlutterWavePayoutStrategy

    private val json = JsonParser()

    @Test
    fun testSendToAccount() {
        val data = """
			{ "account_number": "00118468" }
		""".trimIndent()


        flutterWavePayoutStrategy.payout(PayToDto("account", json.parse(data)), 200, "NGN")
    }

}
