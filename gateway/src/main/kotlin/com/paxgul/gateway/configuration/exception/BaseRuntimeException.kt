package com.paxgul.gateway.configuration.exception

import com.fasterxml.jackson.annotation.JsonIgnoreProperties

@JsonIgnoreProperties("cause", "stackTrace", "message", "localizedMessage", "suppressed")
abstract class BaseRuntimeException : RuntimeException() {
    abstract val error: Enum<*>
    abstract val error_description: String
    override val message: String?
        get() = error_description
    var payload: Any? = null
}
