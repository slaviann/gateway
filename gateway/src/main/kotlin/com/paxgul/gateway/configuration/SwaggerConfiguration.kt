package com.paxgul.gateway.configuration


import com.google.common.base.Predicate
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.util.AntPathMatcher
import springfox.documentation.builders.RequestHandlerSelectors
import springfox.documentation.service.ApiInfo
import springfox.documentation.service.Contact
import springfox.documentation.spi.DocumentationType
import springfox.documentation.spring.web.plugins.Docket

import springfox.documentation.swagger2.annotations.EnableSwagger2


@Configuration
@EnableSwagger2
class SpringFoxConfig {

    @Bean
    fun api(): Docket {

        val paths = listOf(
                "/api/v1/**"
        )

        fun genPredicate(): Predicate<String> = Predicate { input ->
            val matcher = AntPathMatcher()
            paths.any { p -> matcher.match(p, input ?: "") }
        }

        return Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.any())
                .paths(genPredicate())
                .build()
                .apiInfo(apiInfo())
    }


    private fun apiInfo(): ApiInfo {

        val description = """

# Получение доступа до API """.trimIndent()


        return ApiInfo(
                "API",
                description,
                "version-1",
                "API TOS",
                Contact("1", "", ""),
                "API License",
                "API License URL",
                emptyList()
        )
    }

}