package com.paxgul.gateway.bl.exceptions

open class BaseException : RuntimeException() {
    override val message = "Unprocessable request"
}