package com.paxgul.gateway.bl.exceptions

class ObjectNotFoundException(text: String = "Object Not Found"): BaseException() {
    override val message = text
}
