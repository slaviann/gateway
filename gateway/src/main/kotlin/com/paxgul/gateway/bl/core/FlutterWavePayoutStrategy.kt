package com.paxgul.gateway.bl.core

import com.google.gson.Gson
import com.google.gson.JsonElement
import com.squareup.okhttp.MediaType
import com.squareup.okhttp.OkHttpClient
import com.squareup.okhttp.Request
import com.squareup.okhttp.RequestBody
import org.springframework.stereotype.Component
import java.math.BigDecimal


@Component
class FlutterWavePayoutStrategy : PayoutStrategy {

    val BASE_URL = "https://api.flutterwave.com/v3"
    //val BASE_URL = "https://ravesandboxapi.flutterwave.com/v3"

    // prod
    //val SECRET_KEY = "FLWSECK-5b477e61bea6b5bfb91b4ee11915cecf-X"
    // sandbox
    // val SECRET_KEY = "FLWSECK-3ba7d07cc8fb8517bea2ec46893983e7-X"
    // test
    val SECRET_KEY = "FLWSECK_TEST-4766f327a433769459a23c34276d4a05-X"

    private fun transferToAccount(payToDto: PayToDto, amount: Int, currency: String) {

        val element = payToDto.payTo
        val json = element.asJsonObject.apply {
            addProperty("amount", amount)
            addProperty("currency", currency)
            // pay in same currency
            addProperty("debit_currency", currency)
            addProperty("account_bank", "flutterwave")

        }
        println(json.toString())
        val request = Request.Builder()
                .addHeader("Content-Type", "application/json")
                .addHeader("Authorization", "Bearer $SECRET_KEY")
                .url("${BASE_URL}/transfers").post(
                        RequestBody.create(MediaType.parse("application/json"),
                                json.toString()
                        )
                ).build()
        println(OkHttpClient().newCall(request).execute().body()!!.string())
    }

    private fun transferToBankAccount(amount: BigDecimal) {
        val request = Request.Builder()
                .addHeader("Content-Type", "application/json")
                .addHeader("Authorization", "Bearer $SECRET_KEY")
                .url("${BASE_URL}/transfers").post(
                        RequestBody.create(MediaType.parse("application/json"),
                                """{
    "account_bank": "044",
    "account_number": "0690000040",
    "amount": 200,
    "narration": "Akhlm Pstmn Trnsfr xx007",
    "currency": "NGN",
    "reference": "akhlm-pstmnpyt-rfxx007_PMCKDU_2",
    "callback_url": "https://webhook.site/bee0e39e-1bbf-49bf-9504-a931cab366da",
    "debit_currency": "NGN"
    }
                            """.trimIndent()
                        )
                ).build()
        println(OkHttpClient().newCall(request).execute().body()!!.string())
    }

    private fun getAllTransfers() {
        val request = Request.Builder()
                .addHeader("Content-Type", "application/json")
                .addHeader("Authorization", "Bearer $SECRET_KEY")
                .url("${BASE_URL}/transfers").get().build()
        println(OkHttpClient().newCall(request).execute().body()!!.string())
    }

    override fun payout(payToDto: PayToDto, amount: Int, currency: String) {
        if (payToDto.type == "account") {
            transferToAccount(payToDto, amount, currency)
        } else {
            if (payToDto.type != "bank_transfer") return
        }

    }


}



