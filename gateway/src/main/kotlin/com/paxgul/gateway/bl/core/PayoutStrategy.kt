package com.paxgul.gateway.bl.core

import com.google.gson.JsonElement

data class PayToDto(val type: String, val payTo: JsonElement)

interface PayoutStrategy {
    fun payout(payToDto: PayToDto, amount: Int, currency: String)
}